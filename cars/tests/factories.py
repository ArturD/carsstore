import factory

from cars.models import Car, CarMake, CarModel, CarSubmodel


class CarMakeFactory(factory.django.DjangoModelFactory):
    external_id = factory.Faker("uuid4")

    class Meta:
        model = CarMake


class CarModelFactory(factory.django.DjangoModelFactory):
    external_id = factory.Faker("uuid4")
    car_make = factory.SubFactory(CarMakeFactory)

    class Meta:
        model = CarModel


class CarSubmodelFactory(factory.django.DjangoModelFactory):
    external_id = factory.Faker("uuid4")
    car_model = factory.SubFactory(CarModelFactory)

    class Meta:
        model = CarSubmodel


class CarFactory(factory.django.DjangoModelFactory):
    external_id = factory.Faker("uuid4")
    car_submodel = factory.SubFactory(CarSubmodelFactory)
    year = factory.Faker("pyint")
    mileage = factory.Faker("pyint")

    class Meta:
        model = Car
