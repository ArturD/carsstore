from django.urls import reverse
from parameterized import parameterized
from rest_framework.test import APIClient

from cars.serializers import CarSubmodelSerializer
from cars.tests.factories import CarSubmodelFactory
from utils.helpers import snake_to_camel
from utils.tests import BaseApiTestCases


class CarSubmodelTestAPI(
    BaseApiTestCases.BaseGetTest, BaseApiTestCases.BaseGetPaginationTest
):
    url = reverse("carsubmodel-list")
    factory = CarSubmodelFactory
    serializer = CarSubmodelSerializer

    def setUp(self):
        self.client = APIClient()

    @parameterized.expand(
        [
            ["id"],
            ["active"],
            ["created_at"],
            ["updated_at"],
            ["external_id"],
            ["car_model"],
            ["name"],
        ]
    )
    def test_GET_details_returns_CarSubmodel_serialized_data(self, field):
        car_submodel = self.factory.create()
        serialized_data = self.serializer(instance=car_submodel).data
        response = self.client.get(f"{self.url}{car_submodel.id}/")
        data = response.json()
        self.assertEqual(data[snake_to_camel(field)], serialized_data[field])
