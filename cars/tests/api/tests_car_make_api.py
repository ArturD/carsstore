from django.urls import reverse
from parameterized import parameterized
from rest_framework.test import APIClient

from cars.serializers import CarMakeSerializer
from cars.tests.factories import CarMakeFactory
from utils.helpers import snake_to_camel
from utils.tests import BaseApiTestCases


class CarMakeTestAPI(
    BaseApiTestCases.BaseGetTest, BaseApiTestCases.BaseGetPaginationTest
):
    url = reverse("carmake-list")
    factory = CarMakeFactory
    serializer = CarMakeSerializer

    def setUp(self):
        self.client = APIClient()

    @parameterized.expand(
        [
            ["id"],
            ["active"],
            ["created_at"],
            ["updated_at"],
            ["external_id"],
            ["name"],
        ]
    )
    def test_GET_details_returns_CarMake_serialized_data(self, field):
        car_make = self.factory.create()
        serialized_data = self.serializer(instance=car_make).data
        response = self.client.get(f"{self.url}{car_make.id}/")
        data = response.json()
        self.assertEqual(data[snake_to_camel(field)], serialized_data[field])
