from django.urls import reverse
from parameterized import parameterized
from rest_framework.test import APIClient

from cars.serializers import CarModelSerializer
from cars.tests.factories import CarModelFactory
from utils.helpers import snake_to_camel
from utils.tests import BaseApiTestCases


class CarModelTestAPI(
    BaseApiTestCases.BaseGetTest, BaseApiTestCases.BaseGetPaginationTest
):
    url = reverse("carmodel-list")
    factory = CarModelFactory
    serializer = CarModelSerializer

    def setUp(self):
        self.client = APIClient()

    @parameterized.expand(
        [
            ["id"],
            ["active"],
            ["created_at"],
            ["updated_at"],
            ["external_id"],
            ["car_make"],
            ["name"],
        ]
    )
    def test_GET_details_returns_CarMake_serialized_data(self, field):
        car_model = self.factory.create()
        serialized_data = self.serializer(instance=car_model).data
        response = self.client.get(f"{self.url}{car_model.id}/")
        data = response.json()
        self.assertEqual(data[snake_to_camel(field)], serialized_data[field])
