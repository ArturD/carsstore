from django.urls import reverse
from parameterized import parameterized
from rest_framework.test import APIClient

from cars.models import Car
from cars.serializers import CarSerializer
from cars.tests.factories import CarFactory, CarSubmodelFactory
from utils.helpers import snake_to_camel
from utils.tests import BaseApiTestCases


class CarTestAPI(BaseApiTestCases.BaseGetTest, BaseApiTestCases.BaseGetPaginationTest):
    url = reverse("car-list")
    factory = CarFactory
    serializer = CarSerializer

    def setUp(self):
        self.client = APIClient()

    @parameterized.expand(
        [
            ["id"],
            ["active"],
            ["created_at"],
            ["updated_at"],
            ["external_id"],
            ["car_submodel"],
            ["car_submodel_name"],
            ["car_model_name"],
            ["car_make_name"],
            ["price"],
            ["body_type"],
            ["transmission_type"],
            ["fuel_type"],
            ["exterior_color"],
        ]
    )
    def test_GET_details_returns_Car_serialized_data(self, field):
        car_make: Car = CarFactory.create()
        serialized_data = CarSerializer(instance=car_make).data
        response = self.client.get(f"{self.url}{car_make.id}/")
        data = response.json()
        self.assertEqual(data[snake_to_camel(field)], serialized_data[field])

    def test_POST_creates_new_instance(self):
        car_data = {
            "active": True,
            "externalId": "string",
            "year": 1887,
            "mileage": 0,
            "price": 10.00,
            "bodyType": "SEDAN",
            "transmissionType": "AUTOMATIC",
            "fuelType": "PETROL",
            "exteriorColor": "string",
            "carSubmodel": CarSubmodelFactory.create().id,
        }
        response = self.client.post(f"{self.url}", data=car_data)
        data = response.json()
        is_found = Car.objects.filter(id=data["id"]).count()
        self.assertTrue(is_found)

    def test_POST_returns_400_when_adding_car_with_year_below_1886(self):
        car_data = {
            "active": True,
            "externalId": "string",
            "year": 1885,
            "mileage": 0,
            "price": 10.00,
            "bodyType": "SEDAN",
            "transmissionType": "AUTOMATIC",
            "fuelType": "PETROL",
            "exteriorColor": "string",
            "carSubmodel": CarSubmodelFactory.create().id,
        }
        response = self.client.post(f"{self.url}", data=car_data)
        data = response.json()
        self.assertEqual(response.status_code, 400)
        self.assertEqual(
            data["year"], ["Ensure this value is greater than or equal to 1886."]
        )

    def test_POST_returns_400_when_adding_car_with_year_after_3000(self):
        car_data = {
            "active": True,
            "externalId": "string",
            "year": 3001,
            "mileage": 0,
            "price": 10.00,
            "bodyType": "SEDAN",
            "transmissionType": "AUTOMATIC",
            "fuelType": "PETROL",
            "exteriorColor": "string",
            "carSubmodel": CarSubmodelFactory.create().id,
        }
        response = self.client.post(f"{self.url}", data=car_data)
        data = response.json()
        self.assertEqual(response.status_code, 400)
        self.assertEqual(
            data["year"], ["Ensure this value is less than or equal to 3000."]
        )

    def test_POST_returns_400_when_price_is_not_a_number(self):
        car_data = {
            "active": True,
            "externalId": "string",
            "year": 1899,
            "mileage": 0,
            "price": "price",
            "bodyType": "SEDAN",
            "transmissionType": "AUTOMATIC",
            "fuelType": "PETROL",
            "exteriorColor": "string",
            "carSubmodel": CarSubmodelFactory.create().id,
        }
        response = self.client.post(f"{self.url}", data=car_data)
        data = response.json()
        self.assertEqual(response.status_code, 400)
        self.assertEqual(data["price"], ["A valid number is required."])

    def test_POST_returns_400_when_external_id_is_not_unique(self):
        car = CarFactory.create()
        car_data = {
            "active": True,
            "externalId": car.external_id,
            "year": 1899,
            "mileage": 0,
            "price": "price",
            "bodyType": "SEDAN",
            "transmissionType": "AUTOMATIC",
            "fuelType": "PETROL",
            "exteriorColor": "string",
            "carSubmodel": CarSubmodelFactory.create().id,
        }
        response = self.client.post(f"{self.url}", data=car_data)
        data = response.json()
        self.assertEqual(response.status_code, 400)
        self.assertEqual(
            data["externalId"], ["car with this external id already exists."]
        )

    @parameterized.expand(
        [
            ["?min_price=100", "price", 500, 10],
            ["?min_price=600", "price", 500, 0],
            ["?max_price=600", "price", 500, 10],
            ["?max_price=600", "price", 800, 0],
            ["?min_mileage=100", "mileage", 500, 10],
            ["?min_mileage=600", "mileage", 500, 0],
            ["?max_mileage=600", "mileage", 500, 10],
            ["?max_mileage=600", "mileage", 800, 0],
        ]
    )
    def test_GET_returns_filtered_data_for_filter(
        self, lookup, field, value, expected_count
    ):
        kwargs = {field: value}
        CarFactory.create_batch(10, **kwargs)
        response = self.client.get(self.url + lookup)
        data = response.json()
        self.assertEqual(data["count"], expected_count)

    @parameterized.expand(
        [
            ["updated_at"],
            ["-updated_at"],
        ]
    )
    def test_GET_returns_data_in_order_when_ordering_is_passed(self, order_lookup):
        CarFactory.create_batch(10)
        response = self.client.get(self.url + "?ordering=" + order_lookup)
        data = response.json()
        response_data_ids = [x["id"] for x in data["results"]]
        cars = Car.objects.all().order_by(order_lookup)
        cars_ids = [x.id for x in cars]

        self.assertEqual(response_data_ids, cars_ids)
