from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import viewsets

from cars.filters import CarFilter
from cars.models import Car, CarMake, CarModel, CarSubmodel
from cars.serializers import (
    CarMakeSerializer,
    CarModelSerializer,
    CarSerializer,
    CarSubmodelSerializer,
)


class CarMakeViewSet(viewsets.ModelViewSet):
    model = CarMake
    queryset = CarMake.objects.all()
    serializer_class = CarMakeSerializer
    http_method_names = ["get", "head", "options"]


class CarModelViewSet(viewsets.ModelViewSet):
    model = CarModel
    queryset = CarModel.objects.all()
    serializer_class = CarModelSerializer
    http_method_names = ["get", "head", "options"]


class CarSubmodelViewSet(viewsets.ModelViewSet):
    model = CarSubmodel
    queryset = CarSubmodel.objects.all()
    serializer_class = CarSubmodelSerializer
    http_method_names = ["get", "head", "options"]


class CarViewSet(viewsets.ModelViewSet):
    model = Car
    queryset = Car.objects.all()
    serializer_class = CarSerializer
    filter_backends = [DjangoFilterBackend]
    filterset_class = CarFilter
    http_method_names = ["get", "post", "head", "options"]
