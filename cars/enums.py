from utils.enums import ChoicableEnum


class CarBodyType(ChoicableEnum):
    SEDAN = "sedan"
    COUPE = "coupe"
    SUV = "suv"
    VAN = "van"
    CONVERTIBLE = "honvertible"
    HATCHBACK = "hatchback"
    TRUCK = "truck"
    WAGON = "wagon"


class TransmissionType(ChoicableEnum):
    AUTOMATIC = "automatic"
    MANUAL = "manual"


class FuelType(ChoicableEnum):
    PETROL = "petrol"
    HYBRID = "hybrid"
