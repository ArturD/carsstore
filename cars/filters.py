from django_filters import rest_framework as filters, OrderingFilter

from cars.models import Car


class CarFilter(filters.FilterSet):
    min_price = filters.NumberFilter(field_name="price", lookup_expr="gte")
    max_price = filters.NumberFilter(field_name="price", lookup_expr="lte")

    min_mileage = filters.NumberFilter(field_name="mileage", lookup_expr="gte")
    max_mileage = filters.NumberFilter(field_name="mileage", lookup_expr="lte")

    ordering = OrderingFilter(
        # tuple-mapping retains order
        fields=(("updated_at", "updated_at"),),
        # labels do not need to retain order
        field_labels={
            "updated_at": "Updated at",
        },
        help_text="Options: updated_at, -updated_at",
    )

    class Meta:
        model = Car
        fields = ["min_mileage", "max_mileage", "min_price", "max_price"]
