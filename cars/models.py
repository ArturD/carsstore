from django.core.validators import MaxValueValidator, MinValueValidator
from django.db import models

from cars.enums import CarBodyType, FuelType, TransmissionType
from utils.models import ExternalBaseModel


class CarMake(ExternalBaseModel):
    name = models.CharField(max_length=50)


class CarModel(ExternalBaseModel):
    name = models.CharField(max_length=50)
    car_make = models.ForeignKey("cars.CarMake", on_delete=models.CASCADE)


class CarSubmodel(ExternalBaseModel):
    name = models.CharField(max_length=50)
    car_model = models.ForeignKey("cars.CarModel", on_delete=models.CASCADE)


class Car(ExternalBaseModel):
    year = models.IntegerField(
        validators=[MinValueValidator(1886), MaxValueValidator(3000)]
    )
    mileage = models.PositiveIntegerField()
    price = models.DecimalField(max_digits=10, decimal_places=2, null=True, blank=True)
    body_type = models.CharField(
        max_length=20, null=True, blank=True, choices=CarBodyType.choices()
    )
    transmission_type = models.CharField(
        max_length=20, null=True, blank=True, choices=TransmissionType.choices()
    )
    fuel_type = models.CharField(
        max_length=20, null=True, blank=True, choices=FuelType.choices()
    )
    exterior_color = models.CharField(max_length=20, null=True, blank=True)
    car_submodel = models.ForeignKey("cars.CarSubmodel", on_delete=models.CASCADE)

    @property
    def car_submodel_name(self) -> str:
        return self.car_submodel.name

    @property
    def car_model_name(self) -> str:
        return self.car_submodel.car_model.name

    @property
    def car_make_name(self) -> str:
        return self.car_submodel.car_model.car_make.name
