from django.contrib import admin

from cars.models import Car, CarMake, CarModel, CarSubmodel

admin.site.register(CarModel)
admin.site.register(CarSubmodel)
admin.site.register(CarMake)
admin.site.register(Car)
