import decimal
from typing import Optional

from cars.models import Car, CarMake, CarModel, CarSubmodel
from utils.csv_importer import BaseCsvImporter


class BaseCarCsvImporter(BaseCsvImporter):
    @staticmethod
    def get_active_deserializers(value: str) -> bool:
        return True if value == "t" else False


class CarMakeCsvImport(BaseCarCsvImporter):
    model = CarMake
    fields_mappings = {"id": "external_id"}


class CarModelCsvImport(BaseCarCsvImporter):
    model = CarModel
    fields_mappings = {"id": "external_id", "make_id": "car_make_id"}

    def get_car_make_id_deserializer(self, value: str) -> int:
        return CarMake.objects.get(external_id=value).id


class CarSubmodelCsvImport(BaseCarCsvImporter):
    model = CarSubmodel
    csv_file = "../data/submodels.csv"
    fields_mappings = {"id": "external_id", "model_id": "car_model_id"}

    def get_car_model_id_deserializer(self, value: str) -> int:
        return CarModel.objects.get(external_id=value).id


class CarCsvImport(BaseCarCsvImporter):
    model = Car
    fields_mappings = {
        "id": "external_id",
        "submodel_id": "car_submodel_id",
        "transmission": "transmission_type",
    }

    def get_car_submodel_id_deserializer(self, value: str) -> int:
        return CarSubmodel.objects.get(external_id=value).id

    def get_price_deserializer(self, value: str) -> Optional[decimal.Decimal]:
        try:
            return decimal.Decimal(value)
        except decimal.InvalidOperation:
            return None
