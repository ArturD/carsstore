from rest_framework import serializers

from cars.models import Car, CarMake, CarModel, CarSubmodel


class CarMakeSerializer(serializers.ModelSerializer):
    class Meta:
        fields = "__all__"
        model = CarMake


class CarModelSerializer(serializers.ModelSerializer):
    class Meta:
        fields = "__all__"
        model = CarModel


class CarSubmodelSerializer(serializers.ModelSerializer):
    class Meta:
        fields = "__all__"
        model = CarSubmodel


class CarSerializer(serializers.ModelSerializer):
    car_submodel_name = serializers.ReadOnlyField()
    car_model_name = serializers.ReadOnlyField()
    car_make_name = serializers.ReadOnlyField()

    class Meta:
        fields = "__all__"
        model = Car
