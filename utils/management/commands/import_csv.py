from django.core.management.base import BaseCommand, CommandError

from cars.csv_importers import (
    CarMakeCsvImport,
    CarModelCsvImport,
    CarSubmodelCsvImport,
    CarCsvImport,
)


class Command(BaseCommand):
    help = "Imports CSV from given path into database"
    PARSERS = {
        "CarMake": CarMakeCsvImport,
        "CarModel": CarModelCsvImport,
        "CarSubmodel": CarSubmodelCsvImport,
        "Car": CarCsvImport,
    }

    def add_arguments(self, parser):
        parser.add_argument(
            "parser",
            type=str,
            help="Available parsers %s" % [p for p in self.PARSERS.keys()],
        )
        parser.add_argument("file_path", type=str)

    def handle(self, *args, **options):
        try:
            parser = self.PARSERS[options["parser"]]
        except KeyError:
            raise CommandError("Unknown parser, check --help to see available ones")
        csv_importer = parser()
        csv_importer.import_csv(file_path=options["file_path"])
        if csv_importer.failed_rows:
            self.stdout.write(
                self.style.ERROR(
                    "Import failed, check generated csv file to see more details"
                )
            )
        else:
            self.stdout.write(self.style.SUCCESS("Successfully imported data"))
