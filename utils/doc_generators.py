from typing import List

from drf_yasg.inspectors import SwaggerAutoSchema


class CompoundTagsSchema(SwaggerAutoSchema):

    # See https://github.com/axnsan12/drf-yasg/issues/56
    def get_tags(self, operation_keys: List[str]) -> List[str]:
        tags = operation_keys[:-1]
        if len(operation_keys) == 2:
            tags = self.path.split("/")[-3:-2] + tags
        return [" > ".join(tags)]
