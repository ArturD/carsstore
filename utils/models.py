from django.db import models


class BaseModel(models.Model):
    active = models.BooleanField(default=False)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True


class ExternalBaseModel(BaseModel):
    external_id = models.CharField(max_length=100, unique=True)

    class Meta:
        abstract = True
