from django.test import TestCase
from factory import Factory
from parameterized import parameterized
from rest_framework import status
from rest_framework.test import APIClient


class BaseApiTestCases:
    url: str = None
    factory: Factory = None

    class BaseGetTest(TestCase):
        def setUp(self):
            self.client = APIClient()

        def test_GET_returns_STATUS_CODE_200(self):
            response = self.client.get(self.url)
            self.assertEqual(response.status_code, status.HTTP_200_OK)

    class BaseGetPaginationTest(TestCase):
        def setUp(self):
            self.client = APIClient()

        @parameterized.expand(
            [
                ["next"],
                ["previous"],
                ["count"],
                ["results"],
            ]
        )
        def test_GET_returns_pagination_fields(self, field):
            response = self.client.get(self.url)
            data = response.json()
            self.assertIn(field, data)

        def test_GET_returns_paginated_data_default_by_100(self):
            self.factory.create_batch(200)
            response = self.client.get(self.url)
            data = response.json()
            self.assertEqual(len(data["results"]), 100)
            self.assertEqual(data["count"], 200)
