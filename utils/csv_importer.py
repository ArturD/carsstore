import csv
from typing import Any, List, Optional

from django.db import models

from utils.context_managers import suppress_autotime


class BaseCsvImporter:
    model: models.Model = None
    fields_mappings: dict = {}
    csv_file: str = ""
    headers: List[str] = []
    deserializers: dict = {}
    failed_rows: List[List[str]] = []
    added = 0
    total = 0

    def __init__(self, verbose: bool = True, generate_error_file: bool = True) -> None:
        self.verbose = verbose
        self.generate_error_file = generate_error_file
        self._fields_mappings = {
            field.name: field.name for field in self.model._meta.get_fields()
        }
        self._fields_mappings.update(self.fields_mappings)

    def info(self, message: str) -> None:
        if self.verbose:
            print(message)

    def generate_failed_csv(self) -> None:
        csv_split_name = self.csv_file.split(".")
        csv_split_name[-2] += "_failed"
        error_file_name = ".".join(csv_split_name)
        with open(error_file_name, "w", newline="") as csvfile:
            csv_writer = csv.writer(csvfile, delimiter=",", quotechar='"')
            csv_writer.writerow(self.headers + ["original_index", "error"])
            for failed_row in self.failed_rows:
                csv_writer.writerow(failed_row)
        self.info(f"Generated CSV with failed rows {error_file_name}")

    def import_csv(self, file_path: str) -> None:
        self.csv_file = file_path
        with suppress_autotime(self.model, ["created_at", "updated_at"]):
            with open(self.csv_file, newline="") as csvfile:
                csv_reader = csv.reader(csvfile, delimiter=",", quotechar='"')
                self.info(f"Processing import of {self.model.__name__}")
                self.added = 0
                self.total = 0
                for index, row in enumerate(csv_reader):
                    self._handle_raw_row(index, row)

                self._display_summary()
                self._handle_failed_import()

    def _load_headers(self, headers: List[str]) -> None:
        self.headers = headers

    def _get_key(self, column_index: int) -> Optional[str]:
        try:
            model_field = self._fields_mappings.get(self.headers[column_index])
            return model_field
        except IndexError:
            return None

    def _get_value(self, column_index: int, value: Any) -> Optional[Any]:
        model_field = self._get_key(column_index)
        deserializer_function_name = f"get_{model_field}_deserializer"
        deserializer_function = getattr(self, deserializer_function_name, None)
        return deserializer_function(value) if deserializer_function else value

    def _handle_error_row(
        self, row: List, original_index: int, exception: Exception
    ) -> None:
        row.append(original_index)
        row.append(exception)
        self.failed_rows.append(row)

    def _handle_raw_row(self, index: int, row: List[Any]) -> None:
        if index == 0:
            self._load_headers(row)
            return

        raw_object = self._get_raw_object_from_row(row)
        self._create_instance_from_raw_object(raw_object, row, index)
        self.total += 1

    def _create_instance_from_raw_object(
        self, raw_object: dict, row: List[Any], index: int
    ) -> None:
        try:
            self.model.objects.create(**raw_object)
            self.added += 1
        except Exception as exception:
            self._handle_error_row(row, index, exception)

    def _get_raw_object_from_row(self, row: List[Any]) -> dict:
        raw_object = {}
        for column_index, value in enumerate(row):
            key = self._get_key(column_index)
            if key:
                raw_object[key] = self._get_value(column_index, value)
        return raw_object

    def _display_summary(self) -> None:
        self.info(f"Total {self.total}")
        self.info(f"Added {self.added}")
        self.info(f"Failed {len(self.failed_rows)}")

    def _handle_failed_import(self) -> None:
        if self.generate_error_file and self.failed_rows:
            self.generate_failed_csv()
