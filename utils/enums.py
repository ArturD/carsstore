from enum import Enum


class ChoicableEnum(Enum):
    @classmethod
    def choices(cls) -> tuple:
        return tuple((i.name, i.value) for i in cls)
