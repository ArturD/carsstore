import re


def snake_to_camel(text: str) -> str:
    output = "".join(x for x in text.title() if x.isalnum())
    return output[0].lower() + output[1:]


def camel_to_snake(name: str) -> str:
    name = re.sub("(.)([A-Z][a-z]+)", r"\1_\2", name)
    return re.sub("([a-z0-9])([A-Z])", r"\1_\2", name).lower()
