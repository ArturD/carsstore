[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)
[![pipeline status](https://gitlab.com/ArturD/carsstore/badges/master/pipeline.svg)](https://gitlab.com/ArturD/carsstore/-/commits/master)
[![Python 3.8](https://img.shields.io/badge/python-3.8-blue.svg)](https://www.python.org/downloads/release/python-360/)

Code created with: Mypy, flake8, isort, black

## **How to run project**

### **Docker version**

```docker-compose up web``` 

_(this service should be down if you want to use commands below)_

###### Available commands

```docker-compose up import_data``` Import all csv files into database

```docker-compose up flush_db``` Flush db

``docker-compose up test`` Run tests

**Virtualenv version**

Requirments: Python:3.8

1. ```pip install -r requirements.txt```  install packages
2. ```python manage.py migrate``` migrate data
3. ```python manage.py runserver``` run server

###### Available commands

```python manage import_csv```

``` python manage.py import_csv --help
usage: manage.py import_csv [-h] [--version] [-v {0,1,2,3}] [--settings SETTINGS] [--pythonpath PYTHONPATH] [--traceback] [--no-color] [--force-color] [--skip-checks] parser file_path

Imports CSV from given path into database

positional arguments:
  parser                Available parsers ['CarMake', 'CarModel', 'CarSubmodel', 'Car']
  file_path

optional arguments:
  -h, --help            show this help message and exit
  --version             show program's version number and exit
  -v {0,1,2,3}, --verbosity {0,1,2,3}
                        Verbosity level; 0=minimal output, 1=normal output, 2=verbose output, 3=very verbose output
  --settings SETTINGS   The Python path to a settings module, e.g. "myproject.settings.main". If this isn't provided, the DJANGO_SETTINGS_MODULE environment variable will be used.
  --pythonpath PYTHONPATH
                        A directory to add to the Python path, e.g. "/home/djangoprojects/myproject".
  --traceback           Raise on CommandError exceptions
  --no-color            Don't colorize the command output.
  --force-color         Force colorization of the command output.
  --skip-checks         Skip system checks.
```

Examples how to use

```python manage.py import_csv CarMake data/makes.csv```

```python manage.py import_csv CarModel data/models.csv```

```python manage.py import_csv CarSubmodel data/submodels.csv```

```python manage.py import_csv Car data/cars.csv```

_If import fails on any row then script will generate new csv_file with 
failed rows only_

```python manage.py test``` Run all of the tests

```python manage.py flush``` Flush db


### **Access site on**

```http://0.0.0.0:8000/```

```http://0.0.0.0:8000/swagger/``` API description

```http://0.0.0.0:8000/redoc``` API description



_I have created unittest only for API endpoints_