from django.urls import include, path
from rest_framework import routers

from cars.api import CarMakeViewSet, CarModelViewSet, CarSubmodelViewSet, CarViewSet

cars_router = routers.DefaultRouter()
cars_router.register(r"makes", CarMakeViewSet)
cars_router.register(r"models", CarModelViewSet)
cars_router.register(r"submodels", CarSubmodelViewSet)
cars_router.register(r"cars", CarViewSet)

urlpatterns = [
    path("cars/", include(cars_router.urls)),
]
